const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const db = require("./server/utils/connect.db");

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./server/swagger.json");

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var corsOptions = {
  origin: "http://localhost:8081",
};

db.connectToDb();

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", true);
  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  next();
});

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Wish Wheels." });
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

//////////////////COUNTRY///////////////////
require("./server/routes/location/country.route")(app);
require("./server/routes/location/state.route")(app);
require("./server/routes/location/city.route")(app);

//////////////////USER///////////////////
require("./server/routes/user/user.route")(app);
require("./server/routes/user/user.form.route")(app);
require("./server/routes/user/trial.route")(app);
require("./server/routes/user/role.route")(app);
require("./server/routes/user/contact.route")(app);

//////////////////EXTRAS///////////////////
require("./server/routes/extras/logs.route")(app);
require("./server/routes/extras/faq.route")(app);
require("./server/routes/extras/social.media.route")(app);
require("./server/routes/extras/contact.detail.route")(app);
require("./server/routes/extras/comment.route")(app);
require("./server/routes/extras/otp.route")(app);

//////////////////CAR///////////////////
require("./server/routes/car/car.detail.route")(app);
require("./server/routes/car/car.route")(app);
require("./server/routes/car/car.insurance.route")(app);
require("./server/routes/car/car.inventory.route")(app);
require("./server/routes/car/car.equipment.route")(app);
require("./server/routes/car/car.image.route")(app);
require("./server/routes/car/car.model.route")(app);
require("./server/routes/car/car.make.route")(app);
require("./server/routes/car/car.body.route")(app);
require("./server/routes/car/car.sell.route")(app);

//////////////////ADMIN///////////////////
require("./server/routes/admin/admin.route")(app);
require("./server/routes/admin/admin.dashboard.route")(app);

//////////////////DASHBOARD///////////////////
require("./server/routes/dashboard/home.route")(app);
require("./server/routes/dashboard/user.home.route")(app);
