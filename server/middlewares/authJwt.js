const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
const response = require("../utils/response");

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return response.customResponse(res, 404, "No Token Provided", null, null);
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return response.customResponse(res, 404, "Unauthorized", null, null);
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken: verifyToken,
};
module.exports = authJwt;
