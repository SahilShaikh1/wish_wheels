const db = require("../models");
const User = db.user;
const Admin = db.admin;
const response = require("../utils/response");

checkDuplicatePhoneNumberOrEmail = (req, res, next) => {
  // Username
  User.findOne({
    phoneNumber: req.body.phoneNumber,
  }).exec((err, user) => {
    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }

    if (user) {
      response.customResponse(
        res,
        200,
        "Failed! PhoneNumber is already in use!",
        "Failed! PhoneNumber is already in use!",
        null
      );
      return;
    }

    // Email
    User.findOne({
      emailId: req.body.emailId,
    }).exec((err, user) => {
      if (err) {
        response.customResponse(
          res,
          500,
          "Some error occurred while Signin",
          err,
          null
        );
        return;
      }

      if (user) {
        response.customResponse(
          res,
          200,
          "Failed! Email is already in use!",
          "Failed! Email is already in use!",
          null
        );
        return;
      }

      next();
    });
  });
};

checkDuplicatePhoneNumberOrEmailForAdmin = (req, res, next) => {
  // Username
  Admin.findOne({
    phoneNumber: req.body.phoneNumber,
  }).exec((err, user) => {
    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }

    if (user) {
      response.customResponse(
        res,
        200,
        "Failed! PhoneNumber is already in use!",
        "Failed! PhoneNumber is already in use!",
        null
      );
      return;
    }

    // Email
    Admin.findOne({
      emailId: req.body.emailId,
    }).exec((err, user) => {
      if (err) {
        response.customResponse(
          res,
          500,
          "Some error occurred while Signin",
          err,
          null
        );
        return;
      }

      if (user) {
        response.customResponse(
          res,
          200,
          "Failed! Email is already in use!",
          "Failed! Email is already in use!",
          null
        );
        return;
      }

      next();
    });
  });
};

const verifySignUp = {
  checkDuplicatePhoneNumberOrEmail,
  checkDuplicatePhoneNumberOrEmailForAdmin,
};

module.exports = verifySignUp;
