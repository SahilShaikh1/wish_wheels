var Mailer = require("nodemailer");
const response = require("../../utils/response");
const db = require("../../models");
const Otp = db.Otp;
const User = db.user;
const Admin = db.admin;
const config = require("../../config/auth.config");
var jwt = require("jsonwebtoken");
function AddMinutesToDate(date, minutes) {
  return new Date(date.getTime() + minutes * 60000);
}

exports.sendOtp = (req, res) => {
  const now = new Date();
  if (!req.body.userData) {
    response.customResponse(
      res,
      404,
      "Email ID Or Phone Number Required For Sending OTP",
      "Email ID Or Phone Number Required For Sending OTP",
      null
    );
    return;
  }
  User.findOne({
    emailId: req.body.userData,
  }).then((user, err) => {
    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }
    if (!user) {
      return response.customResponse(
        res,
        404,
        "User Not found",
        "User Not found",
        null
      );
    }

    var emailId = req.body.userData;
    var val = Math.floor(100000 + Math.random() * 900000);
    var transportar = Mailer.createTransport({
      service: "gmail",
      auth: {
        user: "wishwheels.tech@gmail.com", // Your Gmail ID
        pass: "nakvfhjpzvvnypyk", // Your Gmail Password
      },
    });

    // Deifne mailing options like Sender Email and Receiver.
    var mailOptions = {
      from: emailId, // Sender ID
      to: emailId, // Reciever ID
      subject: "OTP", // Mail Subject
      html: `<h1>Your OTP IS</h1><p>${val}</p>`, // Description
    };

    // Send an Email
    transportar.sendMail(mailOptions, (error, info) => {
      if (error) {
        response.customResponse(
          res,
          500,
          "Some error occurred while Sending OTP",
          error,
          null
        );
      } else {
        const otp = new Otp({
          otp: val,
          emailId: req.body.userData,
          phoneNumber: req.body.phoneNumber,
          verifiedTill: AddMinutesToDate(now, 3),
          type: "USER",
        });
        otp
          .save(otp)
          .then((data) => {
            console.log(data);
            response.customResponse(
              res,
              200,
              "OTP Sended Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            console.log(err);
            response.customResponse(
              res,
              500,
              "Some error occurred while Sending the OTP",
              err,
              null
            );
          });
      }
    });
  });
};

exports.verifyOtp = (req, res) => {
  if (!req.body.userData) {
    response.customResponse(
      res,
      404,
      "Email ID Or Phone Number Required.",
      "Email ID Or Phone Number Required.",
      null
    );
    return;
  }
  if (!req.body.otp) {
    response.customResponse(res, 404, "OTP Required.", "OTP Required.", null);
    return;
  }
  Otp.findOne({ emailId: req.body.userData, type: "USER" })
    .sort({ createdAt: -1 })
    .limit(1)
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          200,
          "No OTP Record Found",
          "No OTP Record Found",
          null
        );
      } else {
        const now = new Date();
        if (data.otp == req.body.otp) {
          if (now < data.verifiedTill) {
            Otp.find({ emailId: req.body.userData }).remove().exec();
            User.findOne({
              emailId: req.body.userData,
            }).then((user, err) => {
              if (err) {
                response.customResponse(
                  res,
                  500,
                  "Some error occurred while Signin",
                  err,
                  null
                );
                return;
              }

              if (!user) {
                return response.customResponse(
                  res,
                  404,
                  "User Not found",
                  "User Not found",
                  null
                );
              }

              var token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 86400, // 24 hours
              });

              res.status(200).send({
                id: user._id,
                firstName: user.firstName,
                emailId: user.emailId,
                accessToken: token,
              });
            });
          } else {
            Otp.find({ emailId: req.body.userData }).remove().exec();
            response.customResponse(
              res,
              200,
              "OTP Expired",
              "OTP Expired",
              false
            );
          }
        } else {
          response.customResponse(
            res,
            200,
            "Invalid OTP",
            "Invalid OTP",
            false
          );
        }
      }
    })
    .catch((err) => {
      console.log(err);
      response.customResponse(
        res,
        500,
        "Some error occurred while Verifying the Otp.",
        err,
        null
      );
    });
};

exports.sendOtpAdmin = (req, res) => {
  const now = new Date();
  if (!req.body.adminData) {
    response.customResponse(
      res,
      404,
      "Email ID Or Phone Number Required For Sending OTP",
      "Email ID Or Phone Number Required For Sending OTP",
      null
    );
    return;
  }
  Admin.findOne({
    emailId: req.body.adminData,
  }).then((admin, err) => {
    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }
    if (!admin) {
      return response.customResponse(
        res,
        404,
        "Admin Not found",
        "Admin Not found",
        null
      );
    }

    var emailId = req.body.adminData;
    var val = Math.floor(100000 + Math.random() * 900000);
    var transportar = Mailer.createTransport({
      service: "gmail",
      auth: {
        user: "wishwheels.tech@gmail.com", // Your Gmail ID
        pass: "nakvfhjpzvvnypyk", // Your Gmail Password
      },
    });

    // Deifne mailing options like Sender Email and Receiver.
    var mailOptions = {
      from: emailId, // Sender ID
      to: emailId, // Reciever ID
      subject: "OTP", // Mail Subject
      html: `<h1>Your OTP IS</h1><p>${val}</p>`, // Description
    };

    // Send an Email
    transportar.sendMail(mailOptions, (error, info) => {
      if (error) {
        response.customResponse(
          res,
          500,
          "Some error occurred while Sending OTP",
          error,
          null
        );
      } else {
        const otp = new Otp({
          otp: val,
          emailId: req.body.adminData,
          phoneNumber: req.body.phoneNumber,
          verifiedTill: AddMinutesToDate(now, 3),
          type: "ADMIN",
        });
        otp
          .save(otp)
          .then((data) => {
            response.customResponse(
              res,
              200,
              "OTP Sended Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            console.log(err);
            response.customResponse(
              res,
              500,
              "Some error occurred while Sending the OTP",
              err,
              null
            );
          });
      }
    });
  });
};

exports.verifyOtpAdmin = (req, res) => {
  if (!req.body.adminData) {
    response.customResponse(
      res,
      404,
      "Email ID Or Phone Number Required.",
      "Email ID Or Phone Number Required.",
      null
    );
    return;
  }
  if (!req.body.otp) {
    response.customResponse(res, 404, "OTP Required.", "OTP Required.", null);
    return;
  }
  Otp.findOne({ emailId: req.body.adminData, type: "ADMIN" })
    .sort({ createdAt: -1 })
    .limit(1)
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          200,
          "No OTP Record Found",
          "No OTP Record Found",
          null
        );
      } else {
        const now = new Date();
        if (data.otp == req.body.otp) {
          if (now < data.verifiedTill) {
            Otp.find({ emailId: req.body.adminData }).remove().exec();
            Admin.findOne({
              emailId: req.body.adminData,
            }).then((admin, err) => {
              if (err) {
                response.customResponse(
                  res,
                  500,
                  "Some error occurred while Signin",
                  err,
                  null
                );
                return;
              }

              if (!admin) {
                return response.customResponse(
                  res,
                  404,
                  "Admin Not found",
                  "Admin Not found",
                  null
                );
              }

              var token = jwt.sign({ id: admin.id }, config.secret, {
                expiresIn: 86400, // 24 hours
              });

              res.status(200).send({
                id: admin._id,
                firstName: admin.firstName,
                emailId: admin.emailId,
                accessToken: token,
              });
            });
          } else {
            Otp.find({ emailId: req.body.adminData }).remove().exec();
            response.customResponse(
              res,
              200,
              "OTP Expired",
              "OTP Expired",
              false
            );
          }
        } else {
          response.customResponse(
            res,
            200,
            "Invalid OTP",
            "Invalid OTP",
            false
          );
        }
      }
    })
    .catch((err) => {
      console.log(err);
      response.customResponse(
        res,
        500,
        "Some error occurred while Verifying the Otp.",
        err,
        null
      );
    });
};
