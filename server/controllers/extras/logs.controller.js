const db = require("../../models");
const Logs = db.logs;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.eventName) {
    res.status(400).send({ message: "Event Name can not be empty!" });
    return;
  }

  const logs = new Logs({
    eventName: req.body.eventName,
    userId: req.body.userId,
    isActive: true,
  });
  logs
    .save(logs)
    .then((data) => {
      response.customResponse(res, 200, "Log Created Successfully", null, null);
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the Log",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Logs.find({ isActive: true })
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No Log Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Logs Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Logs", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Logs.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No Log Found", null, null);
      else
        response.customResponse(res, 200, "Log Found Successfully", null, data);
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Log", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Logs.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Log Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Log Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Logs", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Logs.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Log Not Found", null, null);
      } else {
        Logs.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Log Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Log", err, null);
    });
};
