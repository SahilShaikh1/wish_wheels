const db = require("../../models");
const FAQ = db.faq;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.question) {
    res.status(400).send({ message: "Question can not be empty!" });
    return;
  }

  const faq = new FAQ({
    question: req.body.question,
    answer: req.body.answer,
    isActive: true,
  });
  faq
    .save(faq)
    .then((data) => {
      response.customResponse(res, 200, "FAQ Created Successfully", null, null);
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the FAQ",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  FAQ.find({ isActive: true })
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No FAQ Found", null, null);
      else
        response.customResponse(res, 200, "FAQ Found Successfully", null, data);
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving FAQ", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  FAQ.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No FAQ Found", null, null);
      else
        response.customResponse(res, 200, "FAQ Found Successfully", null, data);
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving FAQ", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  FAQ.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "FAQ Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "FAQ Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating FAQ", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  FAQ.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "FAQ Not Found", null, null);
      } else {
        FAQ.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "FAQ Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting FAQ", err, null);
    });
};
