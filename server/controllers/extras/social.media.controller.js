const db = require("../../models");
const SocialMedia = db.socialMedia;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Title can not be empty!" });
    return;
  }

  const socialMedia = new SocialMedia({
    title: req.body.title,
    link: req.body.link,
    iconUrl: req.body.iconUrl,
    isActive: true,
  });
  SocialMedia.find({
    isActive: true,
    title: req.body.title,
    link: req.body.link,
  })
    .then((data) => {
      if (data.length >= 1) {
        response.customResponse(
          res,
          200,
          "Social Media Link Already Exisit",
          null,
          null
        );
      } else {
        socialMedia
          .save(socialMedia)
          .then((data) => {
            response.customResponse(
              res,
              200,
              "Social Media Link Saved Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            response.customResponse(
              res,
              500,
              "Some error occurred while saving the social media link.",
              err,
              null
            );
          });
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving the social media link.",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  SocialMedia.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Social Media Account Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Social Media Account Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Social Media Account.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  SocialMedia.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Social Media Account Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Social Media Account Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Social Media Account.",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  SocialMedia.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "No Social Media Account Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Social Media Account Link Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Social Media Account.",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  SocialMedia.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "No Social Media Account Found",
          null,
          null
        );
      } else {
        SocialMedia.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Social Media Account Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Social Media Account.",
        err,
        null
      );
    });
};
