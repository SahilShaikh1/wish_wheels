const db = require("../../models");
const Comment = db.comment;
const Trial = db.trial;
const UserForm = db.userForm;

const response = require("../../utils/response");
const mongoose = require("mongoose");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.comment) {
    res.status(400).send({ message: "Comment can not be empty!" });
    return;
  }

  const comment = new Comment({
    userFormId: req.body.userFormId,
    bookTrialId: req.body.bookTrialId,
    comment: req.body.comment,
    status: req.body.status,
    isActive: true,
  });
  comment
    .save(comment)
    .then((data) => {
      if (req.body.userFormId) {
        UserForm.updateOne(
          { _id: req.body.userFormId },
          {
            status: req.body.status,
          }
        ).then((data) => {
          UserForm.update({
            status: req.body.status,
          });
        });
      } else {
        Trial.updateOne(
          { _id: req.body.bookTrialId },
          {
            status: req.body.status,
          }
        ).then((data) => {
          Trial.update({
            status: req.body.status,
          });
        });
      }
      response.customResponse(
        res,
        200,
        "Comment Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving Comment.",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  let id = mongoose.Types.ObjectId(req.query.id);
  console.log(id);
  Comment.find({ isActive: true, userFormId: id })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Comment Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Comment Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Comment Detail.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Comment.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Comment Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Comment Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Comment Detail.",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Comment.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Comment Detail Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Comment Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Comment Detail.",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Comment.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "No Comment Detail Found",
          null,
          null
        );
      } else {
        Comment.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Comment Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Comment.", err, null);
    });
};
