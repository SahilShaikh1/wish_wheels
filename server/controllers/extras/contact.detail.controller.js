const db = require("../../models");
const ContactDetail = db.contactDetail;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.contactNo) {
    res.status(400).send({ message: "Contact Number can not be empty!" });
    return;
  }

  const contactNo = new ContactDetail({
    contactNo: req.body.contactNo,
    address: req.body.address,
    isActive: true,
  });
  contactNo
    .save(contactNo)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Contact Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving Contact Detail.",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  ContactDetail.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Contact Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Contact Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Contact Detail.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  ContactDetail.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Contact Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Contact Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Contact Detail.",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  ContactDetail.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Contact Detail Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Contact Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Contact Detail.",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  ContactDetail.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Contact Detail Found", null, null);
      } else {
        ContactDetail.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Contact Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Contact Detail.",
        err,
        null
      );
    });
};
