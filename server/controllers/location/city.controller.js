const db = require("../../models");
const City = db.city;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "City Name can not be empty!" });
    return;
  }

  const city = new City({
    name: req.body.name,
    stateId: req.body.stateId,
    isActive: true,
  });
  City.find({ isActive: true, name: req.body.name })
    .then((data) => {
      if (data.length >= 1) {
        response.customResponse(res, 200, "City Already Exisit", null, null);
      } else {
        city
          .save(city)
          .then((data) => {
            response.customResponse(
              res,
              200,
              "City Registered Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            response.customResponse(
              res,
              500,
              "Some error occurred while creating the City",
              err,
              null
            );
          });
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the City",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  City.find({ isActive: true })
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No City Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "City Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving City", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  City.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No City Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "City Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving City", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  City.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "City Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "City Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating City", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  City.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "City Not Found", null, null);
      } else {
        City.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "City Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting City", err, null);
    });
};
