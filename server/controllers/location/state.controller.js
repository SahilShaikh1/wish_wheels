const db = require("../../models");
const State = db.state;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "State Name can not be empty!" });
    return;
  }

  const state = new State({
    name: req.body.name,
    stateCode: req.body.stateCode,
    countryId: req.body.countryId,
    isActive: true,
  });
  state
    .save(state)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "State Registered Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the State",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  State.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No State Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "City Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving State", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  State.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No State Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "State Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving State", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  State.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "State Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "State Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating State", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  State.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "State Not Found", null, null);
      } else {
        State.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "State Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting State", err, null);
    });
};
