const db = require("../../models");
const Country = db.country;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Country Name can not be empty!" });
    return;
  }

  const country = new Country({
    name: req.body.name,
    isActive: true,
  });
  country
    .save(country)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Country Registered Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the Country",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Country.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Country Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "City Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Country", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Country.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Country Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Country Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Country", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Country.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Country Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Country Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Country", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Country.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Country Not Found", null, null);
      } else {
        Country.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Country Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Country", err, null);
    });
};

exports.getCompleteLocationList = (req, res) => {
  Country.aggregate([
    {
      $lookup: {
        from: "states",
        localField: "_id",
        foreignField: "countryId",
        as: "state",
      },
    },
    {
      $unwind: {
        path: "$state",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "cities",
        localField: "state._id",
        foreignField: "stateId",
        as: "state.city",
      },
    },
  ]).then((data) => {
    // console.log(data);
    response.customResponse(res, 200, "City Found Successfully", null, data);
    return;
  });
};
