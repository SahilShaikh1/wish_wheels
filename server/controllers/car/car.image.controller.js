const db = require("../../models");
const CarImage = db.carImage;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.carId) {
    response.customResponse(
      res,
      200,
      "Car Id can not be empty!",
      "Car Id can not be empty!",
      null
    );
    return;
  }
  let files = req.files;
  console.log(req.files);
  console.log(req.body);

  files.forEach((element) => {
    const carImage = new CarImage({
      carId: req.body.carId,
      imageLink: `https://wishwheels.s3.us-east-2.amazonaws.com/${req.body.carId}--${element.originalname}`,
      title: req.body.title,
      isActive: true,
    });
    carImage
      .save(carImage)
      .then((data) => {})
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while saving car Image.",
        });
      });
  });
  response.customResponse(
    res,
    200,
    "Car Image Detail Saved Successfully",
    null,
    null
  );
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarImage.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Image Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Image Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Image",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarImage.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Image Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Image Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Image",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarImage.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Car Image Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Car Image Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Car Image", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarImage.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Car Image Not Found", null, null);
      } else {
        CarImage.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Image Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Car Image", err, null);
    });
};
