const db = require("../../models");
const Car = db.car;
const CarDetail = db.carDetail;
const Logs = db.logs;
const response = require("../../utils/response");
const mongoose = require("mongoose");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId) {
    res.status(400).send({ message: "User Id can not be empty!" });
    return;
  }

  const car = new Car({
    userId: req.body.userId,
    carDetailId: req.body.carDetailId,
    roleId: req.body.roleId,
    isActive: true,
  });

  car
    .save(car)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while saving car detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Car.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Detail Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Car.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Detail Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Car.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Car Detail Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Car Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Car Detail", err, null);
    });
};

exports.delete = (req, res) => {
  let id = req.params.id;

  Car.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      console.log(data);
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Detail Not Found",
          "Car Detail Not Found",
          null
        );
      } else {
        Car.update({
          isActive: false,
        });
        Car.findById(id).then((data) => {
          if (data) {
            CarDetail.updateOne(
              { _id: data.carDetailId },
              {
                isActive: false,
              }
            ).then((data) => {
              response.customResponse(
                res,
                200,
                "Car Detail Deleted Successfully",
                null,
                null
              );
            });
          }
        });
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Car Detail", err, null);
    });
};

exports.getCompleteCarDetailById = (req, res) => {
  let ids = mongoose.Types.ObjectId(req.params.id);
  Car.aggregate([
    { $match: { _id: ids, isActive: true } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "Seller",
      },
    },
    {
      $unwind: {
        path: "$Seller",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_insurances",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Insurance",
      },
    },
    {
      $unwind: {
        path: "$Car_Insurance",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Seller.isActive",
        "Seller.createdAt",
        "Seller.updatedAt",
        "Seller.__v",
        "Seller.cityId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Insurance.isActive",
        "Car_Insurance.createdAt",
        "Car_Insurance.updatedAt",
        "Car_Insurance.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (data.length > 0) {
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
      } else {
        response.customResponse(
          res,
          200,
          "No Car Detail Found With Given Id",
          "No Car Detail Found With Given Id",
          null
        );
      }

      return;
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Fetching Car Detail", err, null);
    });
};

exports.getCompleteCarDetail = (req, res) => {
  Car.aggregate([
    { $match: { isActive: true } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "Seller",
      },
    },
    {
      $unwind: {
        path: "$Seller",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Seller.isActive",
        "Seller.createdAt",
        "Seller.updatedAt",
        "Seller.__v",
        "Seller.cityId",
        "Car_Detail.dateOfPurchase",
        "Car_Detail.transmission",
        "Car_Detail.enginePower",
        "Car_Detail.RTOPassingState",
        "Car_Detail.RTOPassingPinCode",
        "Car_Detail.additionalDetails",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (data.length > 0) {
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
      } else {
        response.customResponse(
          res,
          200,
          "No Car Detail Found",
          "No Car Detail Found",
          null
        );
      }

      return;
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Fetching Car Detail", err, null);
    });
};

exports.getCompleteCarDetailWithFilter = (req, res) => {
  var minYear = req.body.minYear;
  var maxYear = req.body.maxYear;
  var minKm = req.body.minKm;
  var maxKm = req.body.maxKm;
  var minBudget = req.body.minBudget;
  var maxBudget = req.body.maxBudget;
  var body = req.body.body;
  var brand = req.body.brand;
  var match = {
    "Car_Detail.year": { $gte: minYear, $lte: maxYear },
    "Car_Detail.kmDriven": { $gte: minKm, $lte: maxKm },
    "Car_Detail.minPrice": { $gte: minBudget },
    "Car_Detail.maxPrice": { $lte: maxBudget },
  };
  var matchForBrand = {
    "Car_Make.name": { $in: brand },
  };

  var matchForBody = {
    "Car_Body.bodyType": body,
  };

  if (minYear == undefined && maxYear == undefined) {
    delete match["Car_Detail.year"];
  }
  if (minYear == undefined && maxYear != undefined) {
    delete match["Car_Detail.year"];
    match["Car_Detail.year"] = { $lte: maxYear };
  }
  if (minYear != undefined && maxYear == undefined) {
    delete match["Car_Detail.year"];
    match["Car_Detail.year"] = { $gte: minYear };
  }
  if (maxYear != undefined && minYear != undefined) {
    delete match["Car_Detail.year"];
    match["Car_Detail.year"] = { $gte: minYear, $lte: maxYear };
  }
  if (minKm == undefined && maxYear == undefined) {
    delete match["Car_Detail.kmDriven"];
  }
  if (minKm == undefined && maxKm != undefined) {
    delete match["Car_Detail.kmDriven"];
    match["Car_Detail.kmDriven"] = { $lte: maxKm };
  }
  if (minKm != undefined && maxKm == undefined) {
    delete match["Car_Detail.kmDriven"];
    match["Car_Detail.kmDriven"] = { $gte: minKm };
  }
  if (maxKm != undefined && minKm != undefined) {
    delete match["Car_Detail.kmDriven"];
    match["Car_Detail.kmDriven"] = { $gte: minKm, $lte: maxKm };
  }

  if (minBudget == undefined) {
    delete match["Car_Detail.minPrice"];
  }
  if (maxBudget == undefined) {
    delete match["Car_Detail.maxPrice"];
  }

  if (body == undefined) {
    delete matchForBody["Car_Body.bodyType"];
  }

  if (brand == undefined) {
    delete matchForBrand["Car_Make.name"];
  }

  Car.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "Seller",
      },
    },
    {
      $unwind: {
        path: "$Seller",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $match: match,
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $match: matchForBody,
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_insurances",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Insurance",
      },
    },
    {
      $unwind: {
        path: "$Car_Insurance",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $match: matchForBrand,
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Seller.isActive",
        "Seller.createdAt",
        "Seller.updatedAt",
        "Seller.__v",
        "Seller.cityId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Insurance.isActive",
        "Car_Insurance.createdAt",
        "Car_Insurance.updatedAt",
        "Car_Insurance.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (data.length > 0) {
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
      } else {
        response.customResponse(
          res,
          200,
          "No Car Detail Found With Given Filter",
          null,
          null
        );
      }

      return;
    })
    .catch((err) => {
      console.log(err);
      response.customResponse(res, 500, "Error Fetching Car Detail", err, null);
    });
};
