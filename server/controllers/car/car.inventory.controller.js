const db = require("../../models");
const CarInventory = db.carInventory;
const response = require("../../utils/response");
const mongoose = require("mongoose");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.carId) {
    res.status(400).send({ message: "Car Id can not be empty!" });
    return;
  }

  const carInventory = new CarInventory({
    carId: req.body.carId,
    isSold: req.body.isSold,
    isVerified: req.body.isVerified,
    isActive: true,
  });

  carInventory
    .save(carInventory)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Inventory Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while saving car inventory detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  CarInventory.aggregate([
    { $match: { _id: id } },
    {
      $lookup: {
        from: "cars",
        localField: "carId",
        foreignField: "_id",
        as: "Cars",
      },
    },
    {
      $unwind: {
        path: "$Cars",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "Cars.carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Cars.isActive",
        "Cars.createdAt",
        "Cars.updatedAt",
        "Cars.__v",
        "Car_Detail.modelId",
        "Car_Detail.minPrice",
        "Car_Detail.maxPrice",
        "Car_Detail.RTOPassingState",
        "Car_Detail.RTOPassingPinCode",
        "Car_Detail.carMakeId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data || data.length == 0)
        response.customResponse(
          res,
          200,
          "No Car Inventory Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Inventory Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Inventory",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarInventory.aggregate([
    { $match: { isActive: true } },
    {
      $lookup: {
        from: "cars",
        localField: "carId",
        foreignField: "_id",
        as: "Cars",
      },
    },
    {
      $unwind: {
        path: "$Cars",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "Cars.carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Cars.isActive",
        "Cars.createdAt",
        "Cars.updatedAt",
        "Cars.__v",
        "Car_Detail.modelId",
        "Car_Detail.minPrice",
        "Car_Detail.maxPrice",
        "Car_Detail.RTOPassingState",
        "Car_Detail.RTOPassingPinCode",
        "Car_Detail.carMakeId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data || data.length == 0)
        response.customResponse(
          res,
          200,
          "No Car Inventory Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Inventory Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Inventory Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarInventory.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Inventory Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Inventory Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Inventory Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarInventory.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Inventory Detail Not Found",
          null,
          null
        );
      } else {
        CarInventory.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Inventory Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Car Inventory Detail",
        err,
        null
      );
    });
};
