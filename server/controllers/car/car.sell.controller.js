const db = require("../../models");
const CarSell = db.CarSell;
const response = require("../../utils/response");
const mongoose = require("mongoose");
exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId) {
    response.customResponse(
      res,
      200,
      "User Id can not be empty!",
      "User Id can not be empty!",
      null
    );
    return;
  }
  if (!req.body.cityName) {
    response.customResponse(
      res,
      200,
      "City Name can not be empty!",
      "City Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.pincode) {
    response.customResponse(
      res,
      200,
      "Pincode can not be empty!",
      "Pincode can not be empty!",
      null
    );
    return;
  }
  if (!req.body.brandId) {
    response.customResponse(
      res,
      200,
      "Brand Id can not be empty!",
      "Brand Id can not be empty!",
      null
    );
    return;
  }
  if (!req.body.modelId) {
    response.customResponse(
      res,
      200,
      "Model Name can not be empty!",
      "Model Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.year) {
    response.customResponse(
      res,
      200,
      "Year can not be empty!",
      "Year can not be empty!",
      null
    );
    return;
  }
  if (!req.body.ownerShip) {
    response.customResponse(
      res,
      200,
      "Ownership can not be empty!",
      "Ownership can not be empty!",
      null
    );
    return;
  }
  if (!req.body.fuelType) {
    response.customResponse(
      res,
      200,
      "Fuel Type can not be empty!",
      "Fuel Type can not be empty!",
      null
    );
    return;
  }
  if (!req.body.milege) {
    response.customResponse(
      res,
      200,
      "Milege can not be empty!",
      "Milege can not be empty!",
      null
    );
    return;
  }
  if (!req.body.kmDriven) {
    response.customResponse(
      res,
      200,
      "KM Driven can not be empty!",
      "KM Driven can not be empty!",
      null
    );
    return;
  }
  if (!req.body.registrationStateId) {
    response.customResponse(
      res,
      200,
      "State Name can not be empty!",
      "State Name can not be empty!",
      null
    );
    return;
  }

  const carSell = new CarSell({
    userId: req.body.userId,
    cityName: req.body.cityName,
    pincode: req.body.pincode,
    brandId: req.body.brandId,
    modelId: req.body.modelId,
    year: req.body.year,
    ownerShip: req.body.ownerShip,
    fuelType: req.body.fuelType,
    milege: req.body.milege,
    kmDriven: req.body.kmDriven,
    registrationStateId: req.body.registrationStateId,
    isActive: true,
  });

  carSell
    .save(carSell)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while saving car detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  CarSell.aggregate([
    { $match: { isActive: true, _id: id } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "User",
      },
    },
    {
      $unwind: {
        path: "$User",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "brandId",
        foreignField: "_id",
        as: "Brand",
      },
    },
    {
      $unwind: {
        path: "$Brand",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "states",
        localField: "registrationStateId",
        foreignField: "_id",
        as: "Registered_State",
      },
    },
    {
      $unwind: {
        path: "$State",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "User._id",
        "User.isActive",
        "User.createdAt",
        "User.updatedAt",
        "User.__v",
        "Brand.isActive",
        "Brand.createdAt",
        "Brand.updatedAt",
        "Brand.__v",
        "Registered_State.countryId",
        "Registered_State.isActive",
        "Registered_State.createdAt",
        "Registered_State.updatedAt",
        "Registered_State.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Sell Detail Found",
          "No Car Sell Detail Found",
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Sell Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarSell.aggregate([
    { $match: { isActive: true } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "User",
      },
    },
    {
      $unwind: {
        path: "$User",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "brandId",
        foreignField: "_id",
        as: "Brand",
      },
    },
    {
      $unwind: {
        path: "$Brand",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "states",
        localField: "registrationStateId",
        foreignField: "_id",
        as: "Registered_State",
      },
    },
    {
      $unwind: {
        path: "$State",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "User._id",
        "User.isActive",
        "User.createdAt",
        "User.updatedAt",
        "User.__v",
        "Brand.isActive",
        "Brand.createdAt",
        "Brand.updatedAt",
        "Brand.__v",
        "Registered_State.countryId",
        "Registered_State.isActive",
        "Registered_State.createdAt",
        "Registered_State.updatedAt",
        "Registered_State.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Sell Detail Found",
          "No Car Sell Detail Found",
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Sell Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarSell.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Sell Detail Not Found",
          "Car Sell Detail Not Found",
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Sell Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Sell Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarSell.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Sell Detail Not Found",
          "Car Sell Detail Not Found",
          null
        );
      } else {
        Car.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Sell Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Car Sell Detail",
        err,
        null
      );
    });
};
