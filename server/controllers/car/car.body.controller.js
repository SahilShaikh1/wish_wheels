const db = require("../../models");
const CarBody = db.carBody;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.bodyType) {
    res.status(400).send({ message: "Car Body Type can not be empty!" });
    return;
  }

  const carBody = new CarBody({
    bodyType: req.body.bodyType,
    description: req.body.description,
    imageLink: `https://wishwheels.s3.us-east-2.amazonaws.com/${req.body.bodyType}--${req.file.originalname}`,
    isActive: true,
  });

  carBody
    .save(carBody)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Body Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while saving car body detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarBody.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Body Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Body Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Car Body", err, null);
    });
};

exports.findAll = (req, res) => {
  CarBody.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Body Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Body Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Body Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarBody.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Body Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Body Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Body Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarBody.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Body Detail Not Found",
          null,
          null
        );
      } else {
        CarBody.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Body Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Car Body Detail",
        err,
        null
      );
    });
};
