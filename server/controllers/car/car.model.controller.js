const db = require("../../models");
const CarModel = db.carModel;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.modelName) {
    res.status(400).send({ message: "Car Model Name can not be empty!" });
    return;
  }

  const carModel = new CarModel({
    modelName: req.body.modelName,
    modelYear: req.body.modelYear,
    isActive: true,
  });
  CarModel.find({
    isActive: true,
    modelName: req.body.modelName,
    modelYear: req.body.modelYear,
  })
    .then((data) => {
      if (data.length >= 1) {
        response.customResponse(
          res,
          200,
          "Car Model Already Exisit",
          null,
          null
        );
      } else {
        carModel
          .save(carModel)
          .then((data) => {
            response.customResponse(
              res,
              200,
              "Car Model Detail Saved Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            response.customResponse(
              res,
              500,
              "Some error occurred while saving car Model.",
              err,
              null
            );
          });
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving car Model.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarModel.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Model Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Model Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Model Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarModel.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Model Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Model Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Model Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Model Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Model Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Model Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarModel.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Model Detail Not Found",
          null,
          null
        );
      } else {
        CarModel.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Model Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Car Model", err, null);
    });
};
