const db = require("../../models");
const CarDetail = db.carDetail;
const Car = db.car;
const CarInsurance = db.carInsurance;
const CarEquipment = db.carEquipment;
const Logs = db.logs;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Car Name can not be empty!" });
    return;
  }

  const carDetail = new CarDetail({
    name: req.body.name,
    modelId: req.body.modelId,
    carBodyId: req.body.carBodyId,
    year: req.body.year,
    color: req.body.color,
    dateOfPurchase: req.body.dateOfPurchase,
    description: req.body.description,
    kmDriven: req.body.kmDriven,
    carNumber: req.body.carNumber,
    ownerShip: req.body.ownerShip,
    transmission: req.body.transmission,
    fuelType: req.body.fuelType,
    enginePower: req.body.enginePower,
    minPrice: req.body.minPrice,
    maxPrice: req.body.maxPrice,
    RTOPassingState: req.body.RTOPassingState,
    RTOPassingPinCode: req.body.RTOPassingPinCode,
    milege: req.body.milege,
    carMakeId: req.body.carMakeId,
    additionalDetails: req.body.additionalDetails,
    isActive: true,
  });

  carDetail
    .save(carDetail)
    .then((data) => {
      saveCar(data, req, res);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while saving car detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarDetail.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Detail Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarDetail.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Car Detail Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarDetail.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Car Detail Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Car Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Car Detail", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarDetail.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Car Detail Not Found", null, null);
      } else {
        CarDetail.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Car Detail", err, null);
    });
};

// exports.getCompleteCarDetailWithFilter = (req, res) => {
//   var minYear = req.body.minYear;
//   var maxYear = req.body.maxYear;
//   var minKm = req.body.minKm;
//   var maxKm = req.body.maxKm;
//   var minBudget = req.body.minBudget;
//   var maxBudget = req.body.maxBudget;
//   CarDetail.aggregate([
//     {
//       $match: {
//         year: { $gte: minYear },
//         year: { $lte: maxYear },
//         kmDriven: { $gte: minKm },
//         kmDriven: { $lte: maxKm },
//         minPrice: { $gte: minBudget },
//         maxPrice: { $lte: maxBudget },
//       },
//     },
//     {
//       $lookup: {
//         from: "cars",
//         localField: "_id",
//         foreignField: "carDetailId",
//         as: "CarDetail",
//       },
//     },
//     {
//       $unwind: {
//         path: "$CarDetail",
//         preserveNullAndEmptyArrays: true,
//       },
//     },
//     {
//       $lookup: {
//         from: "users",
//         localField: "CarDetail.userId",
//         foreignField: "_id",
//         as: "User",
//       },
//     },
//     {
//       $unwind: {
//         path: "$User",
//         preserveNullAndEmptyArrays: true,
//       },
//     },

//     {
//       $lookup: {
//         from: "car_insurances",
//         localField: "CarDetail.carId",
//         foreignField: "_id",
//         as: "Car_Insurance",
//       },
//     },
//     {
//       $unwind: {
//         path: "$Car_Insurance",
//         preserveNullAndEmptyArrays: true,
//       },
//     },
//     {
//       $lookup: {
//         from: "car_equipments",
//         localField: "CarDetail.carId",
//         foreignField: "_id",
//         as: "Car_Equipments",
//       },
//     },
//     {
//       $lookup: {
//         from: "car_images",
//         localField: "_id",
//         foreignField: "CarDetail.carId",
//         as: "Car_Images",
//       },
//     },
//     {
//       $lookup: {
//         from: "car_makes",
//         localField: "Car_Detail.carMakeId",
//         foreignField: "_id",
//         as: "Car_Make",
//       },
//     },
//     {
//       $unwind: {
//         path: "$Car_Make",
//         preserveNullAndEmptyArrays: true,
//       },
//     },
//     {
//       $lookup: {
//         from: "car_models",
//         localField: "Car_Detail.modelId",
//         foreignField: "_id",
//         as: "Car_Model",
//       },
//     },
//     {
//       $unwind: {
//         path: "$Car_Model",
//         preserveNullAndEmptyArrays: true,
//       },
//     },
//     {
//       $unset: [
//         "isActive",
//         "createdAt",
//         "updatedAt",
//         "__v",
//         "Seller.isActive",
//         "Seller.createdAt",
//         "Seller.updatedAt",
//         "Seller.__v",
//         "Seller.cityId",
//         "Car_Detail.isActive",
//         "Car_Detail.createdAt",
//         "Car_Detail.updatedAt",
//         "Car_Detail.__v",
//         "Car_Insurance.isActive",
//         "Car_Insurance.createdAt",
//         "Car_Insurance.updatedAt",
//         "Car_Insurance.__v",
//         "Car_Equipments.isActive",
//         "Car_Equipments.createdAt",
//         "Car_Equipments.updatedAt",
//         "Car_Equipments.__v",
//         "Car_Images.isActive",
//         "Car_Images.createdAt",
//         "Car_Images.updatedAt",
//         "Car_Images.__v",
//         "Car_Make.isActive",
//         "Car_Make.createdAt",
//         "Car_Make.updatedAt",
//         "Car_Make.__v",
//         "Car_Model.isActive",
//         "Car_Model.createdAt",
//         "Car_Model.updatedAt",
//         "Car_Model.__v",
//       ],
//     },
//   ])
//     .then((data) => {
//       if (data.length > 0) {
//         response.customResponse(
//           res,
//           200,
//           "Car Detail Found Successfully",
//           null,
//           data
//         );
//       } else {
//         response.customResponse(res, 200, "No Car Detail Found", null, null);
//       }

//       return;
//     })
//     .catch((err) => {
//       console.log(err);
//       response.customResponse(res, 500, "Error Fetching Car Detail", err, null);
//     });
// };

function saveCar(data, req, res) {
  const car = new Car({
    userId: req.body.userId,
    carDetailId: data.id,
    roleId: req.body.roleId,
    isActive: true,
  });
  car
    .save(car)
    .then((carData) => {
      console.log("CAR SAVED");
      saveInsurance(carData, req, res);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({
        message: err.message || "Some error occurred while saving car detail.",
      });
    });
}

function saveInsurance(carData, req, res) {
  const carInsurance = new CarInsurance({
    carId: carData.id,
    isInsured: req.body.isInsured,
    insuranceValidTill: req.body.insuranceValidTill,
    isEngineInWarranty: req.body.isEngineInWarranty,
    engineWarrantyValidTill: req.body.engineWarrantyValidTill,
    isTyreInWarranty: req.body.isTyreInWarranty,
    tyreWarrantyValidTill: req.body.tyreWarrantyValidTill,
    isActive: true,
  });

  carInsurance
    .save(carInsurance)
    .then((data) => {
      console.log("CAR INSURANCE SAVED");
      saveCarEquipment(carData, req, res);
    })
    .catch((err) => {
      console.log(err);

      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while saving car insurance detail.",
      });
    });
}

function saveCarEquipment(data, req, res) {
  let number = req.body.equipment.length;
  for (var i = 0; i < req.body.equipment.length; i++) {
    const carEquipment = new CarEquipment({
      carId: data.id,
      equipmentName: req.body.equipment[i].equipmentName,
      description: req.body.equipment[i].description,
      isActive: true,
    });

    carEquipment
      .save(carEquipment)
      .then((data) => {
        console.log("SAVED");
        if (number == i) {
          const logs = new Logs({
            eventName: "New Car Detail Saved By A User",
            userId: req.body.userId,
            isActive: true,
          });
          logs.save(logs).then((data) => {
            response.customResponse(
              res,
              200,
              "Car  Detail Saved Successfully",
              null,
              null
            );
          });
        }
      })
      .catch((err) => {
        console.log(err);

        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while saving car equipment detail.",
        });
      });
  }
}
