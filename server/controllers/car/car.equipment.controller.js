const db = require("../../models");
const CarEquipment = db.carEquipment;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.carId) {
    res.status(400).send({ message: "Car Id can not be empty!" });
    return;
  }

  const carEquipment = new CarEquipment({
    carId: req.body.carId,
    equipmentName: req.body.equipmentName,
    description: req.body.description,
    isActive: true,
  });

  carEquipment
    .save(carEquipment)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Equipment Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while saving car equipment detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarEquipment.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Equipment Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Equipment Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Equipment",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarEquipment.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Equipment Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Equipment Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Equipment Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarEquipment.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Equipment Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Equipment Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Equipment Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarEquipment.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Equipment Detail Not Found",
          null,
          null
        );
      } else {
        CarEquipment.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Equipment Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Car Equipment Detail",
        err,
        null
      );
    });
};
