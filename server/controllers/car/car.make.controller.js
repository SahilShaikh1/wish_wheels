const db = require("../../models");
const CarMake = db.carMake;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Car Make Name can not be empty!" });
    return;
  }

  const carMake = new CarMake({
    name: req.body.name,
    isActive: true,
  });
  CarMake.find({ isActive: true, name: req.body.name })
    .then((data) => {
      if (data.length >= 1) {
        response.customResponse(
          res,
          200,
          "Car Make Already Exisit",
          null,
          null
        );
      } else {
        carMake
          .save(carMake)
          .then((data) => {
            response.customResponse(
              res,
              200,
              "Car Make Detail Saved Successfully",
              null,
              null
            );
          })
          .catch((err) => {
            response.customResponse(
              res,
              500,
              "Some error occurred while saving car Make.",
              err,
              null
            );
          });
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving car Make.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarMake.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Make Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Make Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Make Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarMake.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Make Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Make Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Make Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarMake.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Make Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Make Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Make Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarMake.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Make Detail Not Found",
          null,
          null
        );
      } else {
        CarMake.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Make Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Car Make", err, null);
    });
};
