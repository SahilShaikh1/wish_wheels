const db = require("../../models");
const CarInsurance = db.carInsurance;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.carId) {
    res.status(400).send({ message: "Car Id can not be empty!" });
    return;
  }

  const carInsurance = new CarInsurance({
    carId: req.body.carId,
    isInsured: req.body.isInsured,
    insuranceValidTill: req.body.insuranceValidTill,
    isEngineInWarranty: req.body.isEngineInWarranty,
    engineWarrantyValidTill: req.body.engineWarrantyValidTill,
    isTyreInWarranty: req.body.isTyreInWarranty,
    tyreWarrantyValidTill: req.body.tyreWarrantyValidTill,
    isActive: true,
  });

  carInsurance
    .save(carInsurance)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Car Insurance Detail Saved Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while saving car insurance detail.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  CarInsurance.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Insurance Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Insurance Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Detail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  CarInsurance.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Car Insurance Detail Found",
          null,
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Car Insurance Detail Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Car Insurance Detail",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  CarInsurance.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Insurance Detail Not Found",
          null,
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Car Insurance Detail Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Updating Car Insurance Detail",
        err,
        null
      );
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  CarInsurance.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Car Insurance Detail Not Found",
          null,
          null
        );
      } else {
        CarInsurance.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Car Insurance Detail Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error Deleting Car Insurance Detail",
        err,
        null
      );
    });
};
