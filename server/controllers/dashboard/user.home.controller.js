const db = require("../../models");
const City = db.city;
const Car = db.car;
const Home = db.home;
const response = require("../../utils/response");

exports.userDashBoardData = (req, res) => {
  var cityCount = 0;
  var carCount = 0;

  City.find({ isActive: true })
    .count()
    .then((data) => {
      cityCount = data;
      Car.find({ isActive: true })
        .count()
        .then((data) => {
          carCount = data;

          Home.find()
            .sort({ _id: -1 })
            .limit(1)
            .then((homeData) => {
              var finalData = {
                dashboardData: homeData,
                NoOfCity: cityCount,
                NoOfCar: carCount,
              };
              response.customResponse(res, 200, "Data Found", null, finalData);
            });
        })
        .catch((err) => {
          response.customResponse(
            res,
            500,
            "Something Went Wrong While Retriving Data",
            err,
            null
          );
        });
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Something Went Wrong While Retriving Data",
        err,
        null
      );
    });
};

exports.getLatestArrival = (req, res) => {
  Car.aggregate([
    { $sort: { _id: -1 } },
    { $limit: 3 },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "Seller",
      },
    },
    {
      $unwind: {
        path: "$Seller",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_insurances",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Insurance",
      },
    },
    {
      $unwind: {
        path: "$Car_Insurance",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Seller.isActive",
        "Seller.createdAt",
        "Seller.updatedAt",
        "Seller.__v",
        "Seller.cityId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Insurance.isActive",
        "Car_Insurance.createdAt",
        "Car_Insurance.updatedAt",
        "Car_Insurance.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (data.length > 0) {
        response.customResponse(
          res,
          200,
          "Car Detail Found Successfully",
          null,
          data
        );
      } else {
        response.customResponse(
          res,
          200,
          "No Car Detail Found With Given Id",
          null,
          null
        );
      }

      return;
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Fetching Car Detail", err, null);
    });
};
