const db = require("../../models");
const Home = db.home;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Title can not be empty!" });
    return;
  }

  const home = new Home({
    title: req.body.title,
    subTitle: req.body.subTitle,
    imageLink: req.body.imageLink,
    ctaLabel: req.body.ctaLabel,
    ctaLink: req.body.ctaLink,
    isActive: true,
  });
  home
    .save(home)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Home Data Created Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while saving the Home Data",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Home.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Home Data Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Home Data Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Home Data",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Home.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Home Data Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Home Data Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Home Data",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Home.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Home Data Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Home Data Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Home Data", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Home.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Home Data Not Found", null, null);
      } else {
        Home.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Home Data Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Home Data", err, null);
    });
};
