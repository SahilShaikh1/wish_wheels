const db = require("../../models");
const Contact = db.contact;
const response = require("../../utils/response");
var Mailer = require("nodemailer");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.emailId) {
    response.customResponse(
      res,
      200,
      "Email Id can not be empty!",
      "Email Id can not be empty!",
      null
    );
    return;
  }
  if (!req.body.firstName) {
    response.customResponse(
      res,
      200,
      "First Name can not be empty!",
      "First Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.lastName) {
    response.customResponse(
      res,
      200,
      "Last Name can not be empty!",
      "Last Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.phoneNumber) {
    response.customResponse(
      res,
      200,
      "Phone Number can not be empty!",
      "Phone Number can not be empty!",
      null
    );
    return;
  }
  if (!req.body.cityName) {
    response.customResponse(
      res,
      200,
      "City Name can not be empty!",
      "City Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.query) {
    response.customResponse(
      res,
      200,
      "Query can not be empty!",
      "Query can not be empty!",
      null
    );
    return;
  }
  var emailId = req.body.emailId;
  const contact = new Contact({
    emailId: req.body.emailId,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    phoneNumber: req.body.phoneNumber,
    cityName: req.body.cityName,
    query: req.body.query,
    isActive: true,
  });

  contact
    .save(contact)
    .then((data) => {
      var transportar = Mailer.createTransport({
        service: "gmail",
        auth: {
          user: "wishwheels.tech@gmail.com", // Your Gmail ID
          pass: "nakvfhjpzvvnypyk", // Your Gmail Password
        },
      });

      // Deifne mailing options like Sender Email and Receiver.
      var mailOptions = {
        from: emailId, // Sender ID
        to: emailId, // Reciever ID
        subject: "Thank You", // Mail Subject
        html: `<h1>Thank You</h1><p>for your query we will get back to you soon.</p>`, // Description
      };
      transportar.sendMail(mailOptions, (error, info) => {
        console.log(info);
        console.log(error);
        if (error) {
          response.customResponse(
            res,
            500,
            "Some error occurred while Sending Confirmation Mail",
            error,
            null
          );
        } else {
          response.customResponse(
            res,
            200,
            "Query Registered Successfully",
            null,
            null
          );
        }
      });
    })
    .catch((err) => {
      console.log(err);
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the Query.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Contact.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Query Found",
          "Query Not Found",
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Query Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Query", err, null);
    });
};

exports.findAll = (req, res) => {
  Contact.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Query Found",
          "No Query Found",
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Query Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Query", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Contact.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Query Not Found",
          "Query Not Found",
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Query Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Query", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Contact.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Query Not Found",
          "Query Not Found",
          null
        );
      } else {
        Contact.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Query Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Query", err, null);
    });
};
