const db = require("../../models");
const Trial = db.trial;
const Logs = db.logs;
const response = require("../../utils/response");
const mongoose = require("mongoose");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId) {
    response.customResponse(
      res,
      200,
      "User Id can not be empty!",
      "User Id can not be empty!",
      null
    );
    return;
  }
  if (!req.body.carId) {
    response.customResponse(
      res,
      200,
      "Car Detail can not be empty!",
      "Car Detail can not be empty!",
      null
    );
    return;
  }
  if (!req.body.cityId) {
    response.customResponse(
      res,
      200,
      "City Name can not be empty!",
      "City Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.Address1) {
    response.customResponse(
      res,
      200,
      "Address can not be empty!",
      "Address can not be empty!",
      null
    );
    return;
  }
  if (!req.body.pincode) {
    response.customResponse(
      res,
      200,
      "Pincode can not be empty!",
      "Pincode can not be empty!",
      null
    );
    return;
  }
  if (!req.body.requestPrice) {
    response.customResponse(
      res,
      200,
      "Requested Price can not be empty!",
      "Requested Price can not be empty!",
      null
    );
    return;
  }
  if (!req.body.bookOnDateTime) {
    response.customResponse(
      res,
      200,
      "Booking Date can not be empty!",
      "Booking Date can not be empty!",
      null
    );
    return;
  }
  if (!req.body.status) {
    response.customResponse(
      res,
      200,
      "Status can not be empty!",
      "Status can not be empty!",
      null
    );
    return;
  }

  const trial = new Trial({
    userId: req.body.userId,
    carId: req.body.carId,
    cityId: req.body.cityId,
    Address1: req.body.Address1,
    Address2: req.body.Address2,
    pincode: req.body.pincode,
    requestPrice: req.body.requestPrice,
    bookOnDateTime: req.body.bookOnDateTime,
    status: req.body.status,
    isActive: true,
  });
  trial
    .save(trial)
    .then((data) => {
      const logs = new Logs({
        eventName: "User Has Request Trial.",
        userId: req.body.userId,
        isActive: true,
      });
      logs.save(logs).then((data) => {
        response.customResponse(
          res,
          200,
          "Trial Booked Successfully",
          null,
          null
        );
      });
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while Booking Trail",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Trial.aggregate([
    { $match: { isActive: true } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "User",
      },
    },
    {
      $unwind: {
        path: "$User",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "cars",
        localField: "carId",
        foreignField: "_id",
        as: "Car",
      },
    },
    {
      $unwind: {
        path: "$Car",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "Car.carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "User.isActive",
        "User.createdAt",
        "User.updatedAt",
        "User.__v",

        "Car.isActive",
        "Car.createdAt",
        "Car.updatedAt",
        "Car.__v",
        "Car_Detail.modelId",
        "Car_Detail.ownerShip",
        "Car_Detail.transmission",
        "Car_Detail.enginePower",
        "Car_Detail.minPrice",
        "Car_Detail.maxPrice",
        "Car_Detail.RTOPassingState",
        "Car_Detail.RTOPassingPinCode",
        "Car_Detail.carMakeId",
        "Car_Detail.milege",
        "Car_Detail.additionalDetails",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data)
        response.customResponse(
          res,
          200,
          "No Record Found",
          "No Record Found",
          null
        );
      else
        response.customResponse(
          res,
          200,
          "Record Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Trial", err, null);
    });
};

exports.findByUserId = (req, res) => {
  Trial.find({ isActive: true, userId: req.body.userId })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Record Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Record Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Record", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  Trial.aggregate([
    { $match: { _id: id } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "User_Detail",
      },
    },
    {
      $unwind: {
        path: "$User_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "cars",
        localField: "carId",
        foreignField: "_id",
        as: "Cars",
      },
    },
    {
      $unwind: {
        path: "$Cars",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "Cars.carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_insurances",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Insurance",
      },
    },
    {
      $unwind: {
        path: "$Car_Insurance",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "Cars.isActive",
        "Cars.createdAt",
        "Cars.updatedAt",
        "Cars.__v",
        "User_Detail.isActive",
        "User_Detail.createdAt",
        "User_Detail.updatedAt",
        "User_Detail.__v",
        "User_Detail.cityId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Insurance.isActive",
        "Car_Insurance.createdAt",
        "Car_Insurance.updatedAt",
        "Car_Insurance.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data || data.length == 0)
        response.customResponse(res, 200, "No Record Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Record Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Record", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Trial.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Record Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Record Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Record", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Trial.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Record Not Found", null, null);
      } else {
        Trial.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Record Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Record", err, null);
    });
};
