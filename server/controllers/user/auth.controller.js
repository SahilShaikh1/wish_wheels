const config = require("../../config/auth.config");
const db = require("../../models");
const User = db.user;
const Admin = db.admin;
const response = require("../../utils/response");
const mongoose = require("mongoose");
var jwt = require("jsonwebtoken");

exports.signin = (req, res) => {
  User.findOne({
    phoneNumber: req.body.phoneNumber,
  }).then((user, err) => {
    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }

    if (!user) {
      return response.customResponse(res, 404, "User Not found", null, null);
    }

    var token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 30, // 24 hours
    });

    res.status(200).send({
      id: user._id,
      firstName: user.firstName,
      emailId: user.emailId,
      accessToken: token,
    });
  });
};

exports.signinAdmin = (req, res) => {
  Admin.findOne({
    phoneNumber: req.body.phoneNumber,
  }).then((user, err) => {
    console.log(user);

    if (err) {
      response.customResponse(
        res,
        500,
        "Some error occurred while Signin",
        err,
        null
      );
      return;
    }

    if (!user) {
      return response.customResponse(res, 404, "Admin Not found", null, null);
    }

    var token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 86400, // 24 hours
    });

    res.status(200).send({
      id: user._id,
      firstName: user.firstName,
      emailId: user.emailId,
      accessToken: token,
    });
  });
};

exports.refreshUserToken = (req, res) => {
  const id = req.params.id;

  User.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No User Found", null, null);
      else
        var token = jwt.sign({ id: data.id }, config.secret, {
          expiresIn: 30, // 24 hours
        });
      response.customResponse(
        res,
        200,
        "New Token Generated Successfully",
        null,
        token
      );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving User", err, null);
    });
};

exports.refreshAdminToken = (req, res) => {
  const id = req.params.id;

  Admin.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Admin Found", null, null);
      else
        var token = jwt.sign({ id: data.id }, config.secret, {
          expiresIn: 30, // 24 hours
        });
      response.customResponse(
        res,
        200,
        "New Token Generated Successfully",
        null,
        token
      );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Admin", err, null);
    });
};
