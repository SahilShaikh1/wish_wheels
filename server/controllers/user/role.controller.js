const db = require("../../models");
const Role = db.role;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Role Name can not be empty!" });
    return;
  }

  const role = new Role({
    title: req.body.title,
    description: req.body.description,
    isActive: true,
  });
  role
    .save(role)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Role Created Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the Role",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  Role.find({ isActive: true })
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No Role Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Role Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Role", err, null);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Role.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No Role Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Role Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Role", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Role.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Role Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "Role Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Role", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Role.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "Role Not Found", null, null);
      } else {
        Role.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Role Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Role", err, null);
    });
};
