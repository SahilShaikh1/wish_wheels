const emailjs = require("emailjs-com");
const db = require("../../models");
const User = db.user;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.phoneNumber) {
    response.customResponse(
      res,
      200,
      "PhoneNumber can not be empty!",
      "PhoneNumber can not be empty!",
      null
    );
    return;
  }
  if (!req.body.firstName) {
    response.customResponse(
      res,
      200,
      "First Name can not be empty!",
      "First Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.emailId) {
    response.customResponse(
      res,
      200,
      "Email Id can not be empty!",
      "Email Id can not be empty!",
      null
    );
    return;
  }

  const user = new User({
    emailId: req.body.emailId,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    phoneNumber: req.body.phoneNumber,
    isActive: true,
  });

  user
    .save(user)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "User Registered Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the user.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findById(id)
    .then((data) => {
      if (!data) response.customResponse(res, 200, "No User Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "User Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving User", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "User Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "User Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating User", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  User.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "User Not Found", null, null);
      } else {
        User.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "User Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting User", err, null);
    });
};

exports.otp = (req, res) => {
  var randomOtp = Math.floor(1000 + Math.random() * 9000);
  var templateParams = {
    email: req.body.emailId,
    to_name: "Check this out!",
    message: randomOtp,
  };
  emailjs.init("user_wumXSHOL1jmeFdBmuKVOi");
  emailjs.send("service_8mo0jhj", "template_izdzrrf", templateParams).then(
    function (response) {
      console.log("SUCCESS!", response.status, response.text);
      res.send(randomOtp);
    },
    function (err) {
      console.log("FAILED...", err);
    }
  );
};
