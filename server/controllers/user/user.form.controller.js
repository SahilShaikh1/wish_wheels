const db = require("../../models");
const UserForm = db.userForm;
const Logs = db.logs;
const mongoose = require("mongoose");
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId) {
    res.status(400).send({ message: "User Id can not be empty!" });
    return;
  }

  const userForm = new UserForm({
    userId: req.body.userId,
    carDetailId: req.body.carDetailId,
    type: req.body.type,
    query: req.body.query,
    status: req.body.status,
    isActive: true,
  });
  userForm
    .save(userForm)
    .then((data) => {
      const logs = new Logs({
        eventName: "User Has Raised A Query.",
        userId: req.body.userId,
        isActive: true,
      });
      logs.save(logs).then((data) => {
        response.customResponse(
          res,
          200,
          "User Form Created Successfully",
          null,
          null
        );
      });
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating User Form",
        err,
        null
      );
    });
};

exports.findAll = (req, res) => {
  UserForm.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No User Form Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "User Form Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving User Form",
        err,
        null
      );
    });
};

exports.findByUserId = (req, res) => {
  UserForm.find({ isActive: true, userId: req.body.userId })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No User Form Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "User Form Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving User Form",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  UserForm.aggregate([
    { $match: { _id: id } },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "User_Detail",
      },
    },
    {
      $unwind: {
        path: "$User_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_details",
        localField: "carDetailId",
        foreignField: "_id",
        as: "Car_Detail",
      },
    },
    {
      $unwind: {
        path: "$Car_Detail",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_bodies",
        localField: "Car_Detail.carBodyId",
        foreignField: "_id",
        as: "Car_Body",
      },
    },
    {
      $unwind: {
        path: "$Car_Body",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_insurances",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Insurance",
      },
    },
    {
      $unwind: {
        path: "$Car_Insurance",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_equipments",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Equipments",
      },
    },
    {
      $lookup: {
        from: "car_images",
        localField: "_id",
        foreignField: "carId",
        as: "Car_Images",
      },
    },
    {
      $lookup: {
        from: "car_makes",
        localField: "Car_Detail.carMakeId",
        foreignField: "_id",
        as: "Car_Make",
      },
    },
    {
      $unwind: {
        path: "$Car_Make",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "car_models",
        localField: "Car_Detail.modelId",
        foreignField: "_id",
        as: "Car_Model",
      },
    },
    {
      $unwind: {
        path: "$Car_Model",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unset: [
        "isActive",
        "createdAt",
        "updatedAt",
        "__v",
        "User_Detail.isActive",
        "User_Detail.createdAt",
        "User_Detail.updatedAt",
        "User_Detail.__v",
        "User_Detail.cityId",
        "Car_Detail.isActive",
        "Car_Detail.createdAt",
        "Car_Detail.updatedAt",
        "Car_Detail.__v",
        "Car_Insurance.isActive",
        "Car_Insurance.createdAt",
        "Car_Insurance.updatedAt",
        "Car_Insurance.__v",
        "Car_Equipments.isActive",
        "Car_Equipments.createdAt",
        "Car_Equipments.updatedAt",
        "Car_Equipments.__v",
        "Car_Images.isActive",
        "Car_Images.createdAt",
        "Car_Images.updatedAt",
        "Car_Images.__v",
        "Car_Make.isActive",
        "Car_Make.createdAt",
        "Car_Make.updatedAt",
        "Car_Make.__v",
        "Car_Model.isActive",
        "Car_Model.createdAt",
        "Car_Model.updatedAt",
        "Car_Model.__v",
        "Car_Body.isActive",
        "Car_Body.createdAt",
        "Car_Body.updatedAt",
        "Car_Body.__v",
      ],
    },
  ])
    .then((data) => {
      if (!data || data.length == 0)
        response.customResponse(res, 200, "No User Form Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "User Form Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving User Form",
        err,
        null
      );
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  UserForm.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "User Form Not Found", null, null);
      } else
        response.customResponse(
          res,
          200,
          "User Form Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating User Form", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  UserForm.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(res, 404, "User Form Not Found", null, null);
      } else {
        UserForm.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "User Form Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting User Form", err, null);
    });
};
