const db = require("../../models");
const UserForm = db.userForm;
const Trial = db.trial;
const response = require("../../utils/response");

exports.adminDashBoardData = (req, res) => {
  var activeInspection = 0;
  var activeBooking = 0;
  var activeQuery = 0;

  Trial.find({ isActive: true, status: "Available" })
    .count()
    .then((data) => {
      activeBooking = data;
      UserForm.find({ isActive: true, status: "Available", type: "Sell" })
        .count()
        .then((data) => {
          activeInspection = data;

          UserForm.find({ isActive: true, status: "New", type: "Buy" })
            .count()
            .then((homeData) => {
              activeQuery = data;
              var finalData = {
                activeInspection: activeInspection,
                activeBooking: activeBooking,
                activeQuery: activeQuery,
              };
              response.customResponse(res, 200, "Data Found", null, finalData);
            });
        })
        .catch((err) => {
          response.customResponse(
            res,
            500,
            "Something Went Wrong While Retriving Data",
            err,
            null
          );
        });
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Something Went Wrong While Retriving Data",
        err,
        null
      );
    });
};

exports.findAllInspection = (req, res) => {
  UserForm.find({ isActive: true, type: "Sell" })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Inspection Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Inspections Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Error retrieving Inspections",
        err,
        null
      );
    });
};

exports.findAllQuerys = (req, res) => {
  UserForm.find({ isActive: true, type: "Buy" })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Query Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Queries Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Queries", err, null);
    });
};

exports.findAllBookings = (req, res) => {
  Trial.find({ isActive: true })
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Booking Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Bookings Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Bookings", err, null);
    });
};
