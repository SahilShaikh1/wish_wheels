const db = require("../../models");
const Admin = db.admin;
const response = require("../../utils/response");

exports.create = (req, res) => {
  // Validate request
  if (!req.body.emailId) {
    response.customResponse(
      res,
      200,
      "Email Id can not be empty!",
      "Email Id can not be empty!",
      null
    );
    return;
  }

  if (!req.body.firstName) {
    response.customResponse(
      res,
      200,
      "First Name can not be empty!",
      "First Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.lastName) {
    response.customResponse(
      res,
      200,
      "Last Name can not be empty!",
      "Last Name can not be empty!",
      null
    );
    return;
  }
  if (!req.body.phoneNumber) {
    response.customResponse(
      res,
      200,
      "PhoneNumber can not be empty!",
      "PhoneNumber can not be empty!",
      null
    );
    return;
  }
  if (!req.body.cityId) {
    response.customResponse(
      res,
      200,
      "City can not be empty!",
      "City can not be empty!",
      null
    );
    return;
  }
  if (!req.body.Address1) {
    response.customResponse(
      res,
      200,
      "Address can not be empty!",
      "Address can not be empty!",
      null
    );
    return;
  }
  if (!req.body.roleId) {
    response.customResponse(
      res,
      200,
      "Role can not be empty!",
      "Role can not be empty!",
      null
    );
    return;
  }

  const admin = new Admin({
    emailId: req.body.emailId,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    phoneNumber: req.body.phoneNumber,
    cityId: req.body.cityId,
    pincode: req.body.pincode,
    Address1: req.body.Address1,
    Address2: req.body.Address2,
    roleId: req.body.roleId,
    isActive: true,
  });

  admin
    .save(admin)
    .then((data) => {
      response.customResponse(
        res,
        200,
        "Admin Registered Successfully",
        null,
        null
      );
    })
    .catch((err) => {
      response.customResponse(
        res,
        500,
        "Some error occurred while creating the Admin.",
        err,
        null
      );
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Admin.findById(id)
    .then((data) => {
      if (!data)
        response.customResponse(res, 200, "No Admin Found", null, null);
      else
        response.customResponse(
          res,
          200,
          "Admin Found Successfully",
          null,
          data
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error retrieving Admin", err, null);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return response.customResponse(
      res,
      400,
      "Data to update can not be empty!",
      null,
      null
    );
  }

  const id = req.params.id;

  Admin.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Admin Not Found",
          "Admin Not Found",
          null
        );
      } else
        response.customResponse(
          res,
          200,
          "Admin Updated Successfully",
          null,
          null
        );
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Updating Admin", err, null);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Admin.updateOne(
    { _id: id },
    {
      isActive: false,
    }
  )
    .then((data) => {
      if (!data) {
        response.customResponse(
          res,
          404,
          "Admin Not Found",
          "Admin Not Found",
          null
        );
      } else {
        Admin.update({
          isActive: false,
        });
        response.customResponse(
          res,
          200,
          "Admin Deleted Successfully",
          null,
          null
        );
      }
    })
    .catch((err) => {
      response.customResponse(res, 500, "Error Deleting Admin", err, null);
    });
};
