module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      stateCode: {
        type: String,
        required: true,
      },
      countryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Country",
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const State = mongoose.model("state", schema);
  return State;
};
