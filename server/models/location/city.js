module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      stateId: {
        // type: String,
        type: mongoose.Schema.Types.ObjectId,
        ref: "State",
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const City = mongoose.model("city", schema);
  return City;
};
