module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      emailId: {
        type: String,
        required: true,
      },
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
        required: true,
      },
      phoneNumber: {
        type: String,
        required: true,
      },
      cityId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "City",
        required: true,
      },
      pincode: {
        type: Number,
        required: true,
      },
      Address1: {
        type: String,
        required: true,
      },
      Address2: String,
      roleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Admin = mongoose.model("admin", schema);
  return Admin;
};
