module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      eventName: {
        type: String,
        required: true,
      },
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      dateTime: {
        type: Date,
        default: Date.now,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Logs = mongoose.model("logs", schema);
  return Logs;
};
