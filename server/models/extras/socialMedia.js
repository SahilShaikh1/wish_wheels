module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      title: {
        type: String,
        required: true,
      },
      link: {
        type: String,
        required: true,
      },
      iconUrl: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const SocialMedia = mongoose.model("social_media", schema);
  return SocialMedia;
};
