module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      userFormId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "UserForm",
      },
      bookTrialId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Trial",
      },
      comment: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Comment = mongoose.model("comment", schema);
  return Comment;
};
