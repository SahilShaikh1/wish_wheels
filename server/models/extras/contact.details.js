module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      contactNo: {
        type: String,
        required: true,
      },
      address: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const ContactDetail = mongoose.model("contact_detail", schema);
  return ContactDetail;
};
