module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      otp: {
        type: String,
        required: true,
      },
      emailId: {
        type: String,
      },
      phoneNumber: {
        type: String,
      },
      verifiedTill: {
        type: Date,
        required: true,
      },
      type: {
        type: String,
        required: true,
      },
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Otp = mongoose.model("otp", schema);
  return Otp;
};
