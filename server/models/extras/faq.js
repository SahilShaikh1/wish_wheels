module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      question: {
        type: String,
        required: true,
      },
      answer: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Faq = mongoose.model("faq", schema);
  return Faq;
};
