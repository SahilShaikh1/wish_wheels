module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      carDetailId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarDetail",
        required: true,
      },
      roleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Car = mongoose.model("car", schema);
  return Car;
};
