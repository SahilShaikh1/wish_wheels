module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      carId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car",
        required: true,
      },
      imageLink: {
        type: String,
        required: true,
      },

      title: String,

      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarImage = mongoose.model("car_image", schema);
  return CarImage;
};
