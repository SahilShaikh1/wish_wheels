module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      name: {
        type: String,
        required: true,
      },
      modelId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarModel",
        required: true,
      },
      carBodyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarBody",
        required: true,
      },
      year: {
        type: String,
        required: true,
      },
      color: {
        type: String,
        required: true,
      },
      dateOfPurchase: {
        type: Date,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      kmDriven: {
        type: Number,
        required: true,
      },
      carNumber: {
        type: String,
        required: true,
      },
      ownerShip: {
        type: String,
        required: true,
      },
      transmission: {
        type: String,
        required: true,
      },
      fuelType: {
        type: String,
        required: true,
      },
      enginePower: {
        type: String,
        required: true,
      },
      minPrice: {
        type: Number,
        required: true,
      },
      maxPrice: {
        type: Number,
        required: true,
      },
      RTOPassingState: {
        type: String,
        required: true,
      },
      RTOPassingPinCode: {
        type: String,
        required: true,
      },
      milege: {
        type: String,
        required: true,
      },
      carMakeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarMake",
        required: true,
      },
      additionalDetails: String,
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarDetail = mongoose.model("car_detail", schema);
  return CarDetail;
};
