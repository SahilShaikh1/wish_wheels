module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      carId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car",
        required: true,
      },
      isSold: {
        type: Boolean,
        required: true,
      },
      isVerified: {
        type: Boolean,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarInventory = mongoose.model("car_inventory", schema);
  return CarInventory;
};
