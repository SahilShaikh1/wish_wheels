module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      bodyType: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      imageLink: {
        type: String,
        required: true,
      },

      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarBody = mongoose.model("car_body", schema);
  return CarBody;
};
