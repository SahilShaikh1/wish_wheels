module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true,
      },
      cityName: {
        type: String,
        required: true,
      },
      pincode: {
        type: Number,
        required: true,
      },
      brandId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarMake",
        required: true,
      },
      modelId: {
        type: String,
        required: true,
      },
      year: {
        type: String,
        required: true,
      },
      ownerShip: {
        type: String,
        required: true,
      },
      fuelType: {
        type: String,
        required: true,
      },
      milege: {
        type: String,
        required: true,
      },
      kmDriven: {
        type: String,
        required: true,
      },
      registrationStateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "State",
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarSell = mongoose.model("car_sell", schema);
  return CarSell;
};
