module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      modelName: {
        type: String,
        required: true,
      },
      modelYear: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarModel = mongoose.model("car_model", schema);
  return CarModel;
};
