module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      carId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car",
        required: true,
      },
      isInsured: {
        type: Boolean,
        required: true,
      },
      insuranceValidTill: {
        type: Date,
      },
      isEngineInWarranty: {
        type: Boolean,
        required: true,
      },
      engineWarrantyValidTill: {
        type: Date,
      },
      isTyreInWarranty: {
        type: Boolean,
        required: true,
      },
      tyreWarrantyValidTill: {
        type: Date,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarInsurance = mongoose.model("car_insurance", schema);
  return CarInsurance;
};
