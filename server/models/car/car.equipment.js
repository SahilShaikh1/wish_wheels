module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      carId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car",
        required: true,
      },
      equipmentName: {
        type: String,
        required: true,
      },

      description: String,

      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const CarEquipment = mongoose.model("car_equipment", schema);
  return CarEquipment;
};
