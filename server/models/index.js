const dbConfig = require("../config/db.config");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.user = require("./user/user")(mongoose);
db.country = require("./location/country")(mongoose);
db.state = require("./location/state")(mongoose);
db.city = require("./location/city")(mongoose);
db.userForm = require("./user/userForm")(mongoose);
db.trial = require("./user/trial")(mongoose);
db.role = require("./user/role")(mongoose);
db.logs = require("./extras/logs")(mongoose);
db.faq = require("./extras/faq")(mongoose);
db.socialMedia = require("./extras/socialMedia")(mongoose);
db.contactDetail = require("./extras/contact.details")(mongoose);
db.carDetail = require("./car/car.detail")(mongoose);
db.car = require("./car/car")(mongoose);
db.carInsurance = require("./car/car.insurance")(mongoose);
db.carInventory = require("./car/car.inventory")(mongoose);
db.carEquipment = require("./car/car.equipment")(mongoose);
db.carImage = require("./car/car.image")(mongoose);
db.carModel = require("./car/car.model")(mongoose);
db.carMake = require("./car/car.make")(mongoose);
db.carBody = require("./car/car.body")(mongoose);
db.comment = require("./extras/comment")(mongoose);
db.admin = require("./admin/admin")(mongoose);
db.home = require("./dashboard/home")(mongoose);
db.Otp = require("./extras/otp")(mongoose);
db.CarSell = require("./car/car.sell")(mongoose);
db.contact = require("./user/contact")(mongoose);

module.exports = db;
