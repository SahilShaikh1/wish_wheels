module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      emailId: {
        type: String,
        required: true,
      },
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
        required: true,
      },
      phoneNumber: {
        type: String,
        required: true,
      },
      cityName: {
        type: String,
        required: true,
      },
      query: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Contact = mongoose.model("contact", schema);
  return Contact;
};
