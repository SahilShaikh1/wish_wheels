module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      carDetailId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarDetail",
        required: true,
      },
      type: {
        type: String,
        required: true,
      },
      query: {
        type: String,
        required: true,
      },
      status: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const UserForm = mongoose.model("user_form", schema);
  return UserForm;
};
