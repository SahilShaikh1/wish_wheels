module.exports = (mongoose) => {
  var schema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      carId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car",
        required: true,
      },
      cityId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "City",
        required: true,
      },
      Address1: {
        type: String,
        required: true,
      },
      Address2: String,
      pincode: {
        type: Number,
        required: true,
      },
      requestPrice: {
        type: Number,
        required: true,
      },
      bookOnDateTime: {
        type: Date,
        required: true,
      },
      isConfirmed: {
        type: Boolean,
        required: true,
        default: false,
      },
      status: {
        type: String,
        required: true,
      },
      isActive: Boolean,
    },
    { timestamps: true }
  );

  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Trial = mongoose.model("trial", schema);
  return Trial;
};
