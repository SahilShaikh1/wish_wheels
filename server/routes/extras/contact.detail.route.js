module.exports = (app) => {
  const contactDetail = require("../../controllers/extras/contact.detail.controller");

  var router = require("express").Router();

  router.post("/", contactDetail.create);

  router.get("/", contactDetail.findAll);

  router.get("/:id", contactDetail.findOne);

  router.put("/:id", contactDetail.update);

  router.post("/delete/:id", contactDetail.delete);

  app.use("/api/contactDetail", router);
};
