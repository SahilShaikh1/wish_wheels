module.exports = (app) => {
  const socialMedia = require("../../controllers/extras/social.media.controller");

  var router = require("express").Router();

  router.post("/", socialMedia.create);

  router.get("/", socialMedia.findAll);

  router.get("/:id", socialMedia.findOne);

  router.put("/:id", socialMedia.update);

  router.post("/delete/:id", socialMedia.delete);

  app.use("/api/socialMedia", router);
};
