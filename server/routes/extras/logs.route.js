module.exports = (app) => {
  const logs = require("../../controllers/extras/logs.controller");

  var router = require("express").Router();

  router.post("/", logs.create);

  router.get("/", logs.findAll);

  router.get("/:id", logs.findOne);

  router.put("/:id", logs.update);

  router.post("/delete/:id", logs.delete);

  app.use("/api/logs", router);
};
