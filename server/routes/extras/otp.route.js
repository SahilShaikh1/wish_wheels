module.exports = (app) => {
  const otp = require("../../controllers/extras/otp.controller");

  var router = require("express").Router();

  router.post("/", otp.sendOtp);

  router.post("/verifyOtp", otp.verifyOtp);

  router.post("/sendOtpAdmin", otp.sendOtpAdmin);

  router.post("/verifyOtpAdmin", otp.verifyOtpAdmin);

  app.use("/api/otp", router);
};
