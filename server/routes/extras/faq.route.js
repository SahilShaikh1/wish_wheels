module.exports = (app) => {
  const faq = require("../../controllers/extras/faq.controller");

  var router = require("express").Router();

  router.post("/", faq.create);

  router.get("/", faq.findAll);

  router.get("/:id", faq.findOne);

  router.put("/:id", faq.update);

  router.post("/delete/:id", faq.delete);

  app.use("/api/faq", router);
};
