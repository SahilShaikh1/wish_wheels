module.exports = (app) => {
  const comment = require("../../controllers/extras/comment.controller");

  var router = require("express").Router();

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  router.post("/", [authJwt.verifyToken], comment.create);

  router.get("/", [authJwt.verifyToken], comment.findAll);

  router.get("/:id", [authJwt.verifyToken], comment.findOne);

  router.put("/:id", [authJwt.verifyToken], comment.update);

  router.post("/delete/:id", [authJwt.verifyToken], comment.delete);

  app.use("/api/comment", router);
};
