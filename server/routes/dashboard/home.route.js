module.exports = (app) => {
  const home = require("../../controllers/dashboard/home.controller");

  var router = require("express").Router();

  router.post("/", home.create);

  router.get("/", home.findAll);

  router.get("/:id", home.findOne);

  router.put("/:id", home.update);

  router.post("/delete/:id", home.delete);

  app.use("/api/home", router);
};
