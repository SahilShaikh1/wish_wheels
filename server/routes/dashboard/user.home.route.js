module.exports = (app) => {
  const userHome = require("../../controllers/dashboard/user.home.controller");

  var router = require("express").Router();

  router.get("/", userHome.userDashBoardData);

  router.get("/getLatestArrival", userHome.getLatestArrival);

  app.use("/api/userHome", router);
};
