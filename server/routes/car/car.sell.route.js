module.exports = (app) => {
  const car = require("../../controllers/car/car.sell.controller");

  var router = require("express").Router();

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  router.post("/", [authJwt.verifyToken], car.create);

  router.get("/", [authJwt.verifyToken], car.findAll);

  router.get("/:id", [authJwt.verifyToken], car.findOne);

  router.put("/:id", [authJwt.verifyToken], car.update);

  router.post("/delete/:id", [authJwt.verifyToken], car.delete);

  app.use("/api/sellCar", router);
};
