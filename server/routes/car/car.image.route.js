module.exports = (app) => {
  const carImage = require("../../controllers/car/car.image.controller");
  const uploadFile = require("../../middlewares/imageUpload");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post(
    "/",
    [authJwt.verifyToken],
    uploadFile.array("image", 5),
    carImage.create
  );

  router.get("/", carImage.findAll);

  router.get("/:id", carImage.findOne);

  router.put("/:id", [authJwt.verifyToken], carImage.update);

  router.post("/delete/:id", [authJwt.verifyToken], carImage.delete);

  app.use("/api/carImage", router);
};
