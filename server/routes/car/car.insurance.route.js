module.exports = (app) => {
  const carInsurance = require("../../controllers/car/car.insurance.controller");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post("/", [authJwt.verifyToken], carInsurance.create);

  router.get("/", carInsurance.findAll);

  router.get("/:id", carInsurance.findOne);

  router.put("/:id", [authJwt.verifyToken], carInsurance.update);

  router.post("/delete/:id", [authJwt.verifyToken], carInsurance.delete);

  app.use("/api/carInsurance", router);
};
