module.exports = (app) => {
  const carMake = require("../../controllers/car/car.make.controller");

  var router = require("express").Router();

  router.post("/", carMake.create);

  router.get("/", carMake.findAll);

  router.get("/:id", carMake.findOne);

  router.put("/:id", carMake.update);

  router.post("/delete/:id", carMake.delete);

  app.use("/api/carMake", router);
};
