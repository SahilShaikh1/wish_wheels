module.exports = (app) => {
  const carDetail = require("../../controllers/car/car.detail.controller");

  var router = require("express").Router();

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  router.post("/", carDetail.create);

  router.get("/", [authJwt.verifyToken], carDetail.findAll);

  router.get("/:id", carDetail.findOne);

  router.put("/:id", carDetail.update);

  router.post("/delete/:id", [authJwt.verifyToken], carDetail.delete);

  app.use("/api/carDetail", router);
};
