module.exports = (app) => {
  const carEquipment = require("../../controllers/car/car.equipment.controller");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post("/", [authJwt.verifyToken], carEquipment.create);

  router.get("/", carEquipment.findAll);

  router.get("/:id", carEquipment.findOne);

  router.put("/:id", [authJwt.verifyToken], carEquipment.update);

  router.post("/delete/:id", [authJwt.verifyToken], carEquipment.delete);

  app.use("/api/carEquipment", router);
};
