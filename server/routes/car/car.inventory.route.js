module.exports = (app) => {
  const carInventory = require("../../controllers/car/car.inventory.controller");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post("/", [authJwt.verifyToken], carInventory.create);

  router.get("/", carInventory.findAll);

  router.get("/:id", carInventory.findOne);

  router.put("/:id", [authJwt.verifyToken], carInventory.update);

  router.post("/delete/:id", [authJwt.verifyToken], carInventory.delete);

  app.use("/api/carInventory", router);
};
