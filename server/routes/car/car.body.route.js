module.exports = (app) => {
  const carBody = require("../../controllers/car/car.body.controller");
  const uploadFile = require("../../middlewares/carBodyImageUpload");
  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post(
    "/",
    [authJwt.verifyToken],
    uploadFile.single("image"),
    carBody.create
  );

  router.get("/", carBody.findAll);

  router.get("/:id", carBody.findOne);

  router.put("/:id", [authJwt.verifyToken], carBody.update);

  router.post("/delete/:id", [authJwt.verifyToken], carBody.delete);

  app.use("/api/carBody", router);
};
