module.exports = (app) => {
  const carModel = require("../../controllers/car/car.model.controller");

  var router = require("express").Router();

  router.post("/", carModel.create);

  router.get("/", carModel.findAll);

  router.get("/:id", carModel.findOne);

  router.put("/:id", carModel.update);

  router.post("/delete/:id", carModel.delete);

  app.use("/api/carModel", router);
};
