module.exports = (app) => {
  const car = require("../../controllers/car/car.controller");

  var router = require("express").Router();

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  router.post("/", [authJwt.verifyToken], car.create);

  router.get("/", car.findAll);

  router.get("/:id", car.findOne);

  router.get("/complete/car/detail", car.getCompleteCarDetail);

  router.get("/completeCarDetailById/:id", car.getCompleteCarDetailById);

  router.put("/:id", [authJwt.verifyToken], car.update);

  router.post("/delete/:id", car.delete);

  router.post("/filter/car/detail", car.getCompleteCarDetailWithFilter);

  app.use("/api/car", router);
};
