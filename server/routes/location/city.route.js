module.exports = (app) => {
  const city = require("../../controllers/location/city.controller");

  var router = require("express").Router();

  router.post("/", city.create);

  router.get("/", city.findAll);

  router.get("/:id", city.findOne);

  router.put("/:id", city.update);

  router.post("/delete/:id", city.delete);

  app.use("/api/city", router);
};
