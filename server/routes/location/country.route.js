module.exports = (app) => {
  const country = require("../../controllers/location/country.controller");

  var router = require("express").Router();

  router.post("/", country.create);

  router.get("/", country.findAll);

  router.get("/:id", country.findOne);

  router.put("/:id", country.update);

  router.post("/delete/:id", country.delete);

  //Gives you complete List with country,states and cities
  router.get("/complete/locationList", country.getCompleteLocationList);

  app.use("/api/country", router);
};
