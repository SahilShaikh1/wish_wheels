module.exports = (app) => {
  const state = require("../../controllers/location/state.controller");

  var router = require("express").Router();

  router.post("/", state.create);

  router.get("/", state.findAll);

  router.get("/:id", state.findOne);

  router.put("/:id", state.update);

  router.post("/delete/:id", state.delete);

  app.use("/api/state", router);
};
