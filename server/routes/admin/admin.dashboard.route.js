module.exports = (app) => {
  const adminDashboard = require("../../controllers/admin/admin.dashboard.controller");
  const { authJwt } = require("../../middlewares");

  var router = require("express").Router();
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  router.get("/", [authJwt.verifyToken], adminDashboard.adminDashBoardData);

  router.get(
    "/findAllInspection",
    [authJwt.verifyToken],
    adminDashboard.findAllInspection
  );

  router.get(
    "/findAllQuerys",
    [authJwt.verifyToken],
    adminDashboard.findAllQuerys
  );

  router.get(
    "/findAllBookings",
    [authJwt.verifyToken],
    adminDashboard.findAllBookings
  );

  app.use("/api/adminDashboard", router);
};
