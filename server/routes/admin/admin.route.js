module.exports = (app) => {
  const admin = require("../../controllers/admin/admin.controller");
  const adminAuth = require("../../controllers/user/auth.controller");
  const { verifySignUp } = require("../../middlewares");
  const { authJwt } = require("../../middlewares");

  var router = require("express").Router();

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // Create a new User
  router.post(
    "/",
    [verifySignUp.checkDuplicatePhoneNumberOrEmailForAdmin],
    admin.create
  );

  router.get("/:id", [authJwt.verifyToken], admin.findOne);

  router.put("/:id", [authJwt.verifyToken], admin.update);

  router.post("/signin", adminAuth.signinAdmin);

  router.post("/delete/:id", [authJwt.verifyToken], admin.delete);

  router.get("/refreshToken/:id", adminAuth.refreshAdminToken);

  app.use("/api/admin", router);
};
