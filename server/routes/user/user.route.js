module.exports = (app) => {
  const users = require("../../controllers/user/user.controller");
  const usersAuth = require("../../controllers/user/auth.controller");
  const { verifySignUp } = require("../../middlewares");
  const { authJwt } = require("../../middlewares");

  var router = require("express").Router();

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // Create a new User
  router.post(
    "/",
    [verifySignUp.checkDuplicatePhoneNumberOrEmail],
    users.create
  );

  router.get("/:id", [authJwt.verifyToken], users.findOne);

  router.put("/:id", [authJwt.verifyToken], users.update);

  router.post("/signin", usersAuth.signin);

  router.post("/delete/:id", users.delete);

  router.get("/refreshToken/:id", usersAuth.refreshUserToken);

  router.post("/otp", users.otp);

  app.use("/api/users", router);
};
