module.exports = (app) => {
  const trial = require("../../controllers/user/trial.controller");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post("/", [authJwt.verifyToken], trial.create);

  router.get("/", trial.findAll);

  router.post("/findByUserId", [authJwt.verifyToken], trial.findByUserId);

  router.get("/:id", [authJwt.verifyToken], trial.findOne);

  router.put("/:id", [authJwt.verifyToken], trial.update);

  router.post("/delete/:id", [authJwt.verifyToken], trial.delete);

  app.use("/api/trial", router);
};
