module.exports = (app) => {
  const contact = require("../../controllers/user/contact.controller");

  var router = require("express").Router();

  // Create a new contact
  router.post("/", contact.create);

  router.get("/", contact.findAll);

  router.get("/:id", contact.findOne);

  router.put("/:id", contact.update);

  router.post("/delete/:id", contact.delete);

  app.use("/api/contact", router);
};
