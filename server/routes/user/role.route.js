module.exports = (app) => {
  const role = require("../../controllers/user/role.controller");

  var router = require("express").Router();

  router.post("/", role.create);

  router.get("/", role.findAll);

  router.get("/:id", role.findOne);

  router.put("/:id", role.update);

  router.post("/delete/:id", role.delete);

  app.use("/api/role", router);
};
