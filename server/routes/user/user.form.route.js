module.exports = (app) => {
  const userForm = require("../../controllers/user/user.form.controller");

  const { authJwt } = require("../../middlewares");

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  router.post("/", [authJwt.verifyToken], userForm.create);

  router.get("/", userForm.findAll);

  router.post(
    "/getUserFormByUserId",
    [authJwt.verifyToken],
    userForm.findByUserId
  );

  router.get("/:id", [authJwt.verifyToken], userForm.findOne);

  router.put("/:id", [authJwt.verifyToken], userForm.update);

  router.post("/delete/:id", [authJwt.verifyToken], userForm.delete);

  app.use("/api/userForm", router);
};
