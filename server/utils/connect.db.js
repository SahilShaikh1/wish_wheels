const db = require("../models");

function connectToDb() {
  db.mongoose
    .connect(db.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log("Connected to the database!");
    })
    .catch((err) => {
      console.log("Cannot connect to the database!", err);
      process.exit();
    });
}

function clearDb() {
  const mongodbconnection = require("mongoose");
  mongodbconnection.connect(db.url, {
    useNewUrlParser: true,
  });
  const connection = mongodbconnection.connection;
  connection.once("open", function () {
    console.log("*** MongoDB got connected ***");
    console.log(`Our Current Database Name : ${connection.db.databaseName}`);
    mongodbconnection.connection.db.dropDatabase(
      console.log(`${connection.db.databaseName} database dropped.`)
    );
  });
}

module.exports = { connectToDb, clearDb };
