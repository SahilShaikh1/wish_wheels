function customResponse(res, status, message, error, data) {
  res.status(status).send({
    status: status,
    message: message,
    error: error,
    data: data,
  });
}

module.exports = { customResponse };
