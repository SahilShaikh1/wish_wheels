var moment = require("moment");

function getCurrentDate() {
  var date = new Date();
  var currentTimeInUTC = date.getTime();
  var currentTimeInIST = moment(currentTimeInUTC)
    .utcOffset("+05:30")
    .format("YYYY/MM/DD");
  return currentTimeInIST;
}

function getCurrentTime() {
  var date = new Date();
  var currentTimeInUTC = date.getTime();
  var currentTimeInIST = moment(currentTimeInUTC)
    .utcOffset("+05:30")
    .format("HH:mm:ss");
  return currentTimeInIST;
}
module.exports = { getCurrentTime, getCurrentDate };
